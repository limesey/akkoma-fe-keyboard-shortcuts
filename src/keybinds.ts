// Main file

// Brainstorm:
// Mutation observer JUST for waiting for the TL to load?
// Then a second observer to monitor the TL, detecting new posts being loaded
// should hopefully be more efficient and checking if every element added to the document is a post

console.log("Keybinds script loaded.");

let timeline = document.querySelector("div.timeline");

const OBSERVER = new MutationObserver(onBodyMutationObserved);
const TIMELINE_OBSERVER = new MutationObserver(onTimelineMutationObserved);

function onTimelineMutationObserved() {}

/**
 * Waits for the timeline div to be loaded into the DOM and initiates a new observer detecting mutations within the user's TL
 * @param mutationList
 * @param observer
 */
function onBodyMutationObserved(
  mutationList: MutationRecord[],
  observer: MutationObserver
) {
  const filtered = mutationList.filter(
    (mutation) => mutation.type == "childList" && mutation.addedNodes.length > 0
  );

  for (const mutation of filtered) {
    for (const childAdded of mutation.addedNodes) {
      const element = childAdded as HTMLElement;

      if (element.classList.contains("timeline")) {
        timeline = element;

        observer.disconnect();

        TIMELINE_OBSERVER.observe(timeline, { childList: true });
      }
    }
  }
}

OBSERVER.observe(document.body, { childList: true, subtree: true });
