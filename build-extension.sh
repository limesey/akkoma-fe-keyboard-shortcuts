
function logInfo {
    echo "info: $1"
}

# TODO: Prettier formatting/output
function logError {
    echo "error: Failed to build: $1"
}


# TODO: Check if dir already exists
rm -rf dist

# TODO: Check for errors before proceeding
tsc

cp src/manifest.json dist/

logInfo "Extension built."